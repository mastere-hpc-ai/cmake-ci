#include "Hello.h"
#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char *argv[]) {
  std::unique_ptr<Hello> hello;

  try {
    namespace po = boost::program_options;
    po::options_description desc("Allowed options");
    desc.add_options()("help", "help message")(
        "greeting", po::value<std::string>()->default_value("Hello"),
        "Greeting to use")("name", po::value<std::string>(), "Name to display");

    po::positional_options_description pd;
    pd.add("name", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << "Usage: " << argv[0] << "[options] [<name>]\n";
      std::cout << desc << "\n";
      return 0;
    }

    // greeting always defined (has a default value)
    hello = std::make_unique<Hello>(vm["greeting"].as<std::string>());

    if (vm.count("name")) {
      hello->say(vm["name"].as<std::string>());
    } else {
      std::cerr << "No name has been set.\n";
      return 1;
    }
  } catch (std::exception &e) {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  } catch (...) {
    std::cerr << "Exception of unknown type!\n";
  }
}
