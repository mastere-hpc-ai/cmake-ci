#ifndef HELLO_H
#define HELLO_H

#include <string>

class Hello {
public:
  Hello(std::string  greeting);
  void say(const std::string & name) const;
private:
  std::string m_greeting;
};

#endif /* HELLO_H */
